/******************************************************************************
 Atmel - C driver - UART
   - UART driver, implemented in C for Atmel microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** UART/USART with IRQ, Interface 3 driver.
 *
 * @file      uart3_irq.c
 * @see       uart3_irq.h
 * @see       uart_irq.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


// ISR
#define USARTx_RX_vect USART3_RX_vect
#define USARTx_UDRE_vect USART3_UDRE_vect

// Functions
#define uart_irq_init uart3_irq_init

#define uart_irq_transmit_byte       uart3_irq_transmit_byte
#define uart_irq_receive_byte_wait   uart3_irq_receive_byte_wait
#define uart_irq_receive_byte_nowait uart3_irq_receive_byte_nowait

#define uart_irq_putchar        uart3_irq_putchar
#define uart_irq_getchar_wait   uart3_irq_getchar_wait
#define uart_irq_getchar_nowait uart3_irq_getchar_nowait

// Register
#define UBRRxH UBRR3H
#define UBRRxL UBRR3L

#define UCSRxA UCSR3A
#define UCSRxB UCSR3B
#define UCSRxC UCSR3C

#define UDRx UDR3

// Bits
#define RXENx RXEN3
#define TXENx TXEN3
#define USBSx USBS3
#define UCSZx0 UCSZ30
#define UCSZx1 UCSZ31
#define RXCIEx RXCIE3
#define UDRIEx UDRIE3

#define RXCx RXC3
#define TXCx TXC3


#include "uart3_irq.h"
#include "uart_irq.c"

