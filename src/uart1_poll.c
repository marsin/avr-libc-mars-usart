/******************************************************************************
 Atmel - C driver - UART
   - UART driver, implemented in C for Atmel microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Polling UART/USART, Interface 1 driver.
 *
 * @file      uart1_poll.c
 * @see       uart1_poll.h
 * @see       uart_poll.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


// Functions
#define uart_poll_init uart1_poll_init

#define uart_poll_transmit_byte       uart1_poll_transmit_byte
#define uart_poll_receive_byte_wait   uart1_poll_receive_byte_wait
#define uart_poll_receive_byte_nowait uart1_poll_receive_byte_nowait

#define uart_poll_putchar        uart1_poll_putchar
#define uart_poll_getchar_wait   uart1_poll_getchar_wait
#define uart_poll_getchar_nowait uart1_poll_getchar_nowait

// Register
#define UBRRxH UBRR1H
#define UBRRxL UBRR1L

#define UCSRxA UCSR1A
#define UCSRxB UCSR1B
#define UCSRxC UCSR1C

#define UDRx UDR1

// Bits
#define RXENx RXEN1
#define TXENx TXEN1
#define USBSx USBS1
#define UCSZx0 UCSZ10

#define UDREx UDRE1
#define RXCx RXC1


#include "uart1_poll.h"
#include "uart_poll.c"

