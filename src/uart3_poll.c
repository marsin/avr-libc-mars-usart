/******************************************************************************
 Atmel - C driver - UART
   - UART driver, implemented in C for Atmel microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Polling UART/USART, Interface 3 driver.
 *
 * @file      uart3_poll.c
 * @see       uart3_poll.h
 * @see       uart_poll.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


// Functions
#define uart_poll_init uart3_poll_init

#define uart_poll_transmit_byte       uart3_poll_transmit_byte
#define uart_poll_receive_byte_wait   uart3_poll_receive_byte_wait
#define uart_poll_receive_byte_nowait uart3_poll_receive_byte_nowait

#define uart_poll_putchar        uart3_poll_putchar
#define uart_poll_getchar_wait   uart3_poll_getchar_wait
#define uart_poll_getchar_nowait uart3_poll_getchar_nowait

// Register
#define UBRRxH UBRR3H
#define UBRRxL UBRR3L

#define UCSRxA UCSR3A
#define UCSRxB UCSR3B
#define UCSRxC UCSR3C

#define UDRx UDR3

// Bits
#define RXENx RXEN3
#define TXENx TXEN3
#define USBSx USBS3
#define UCSZx0 UCSZ30

#define UDREx UDRE3
#define RXCx RXC3


#include "uart3_poll.h"
#include "uart_poll.c"

