/******************************************************************************
 Atmel - C driver - UART
   - UART driver, implemented in C for Atmel microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** UART/USART IRQ interface driver.
 *
 * @file      uart_irq.c
 * @file      uart0_irq.h
 * @file      uart0_irq.c
 * @see       uart_utils.h
 * @see       uart_utils.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 *
 *
 * About
 * -----
 *
 * This implementation bases on:
 * <http://rn-wissen.de/wiki/index.php?title=UART_mit_avr-gcc>
 *
 *
 * About Baud
 * ----------
 *
 * Baud is the unit for symbol rate or modulation rate in symbols per second or pulses per second.
 *   -- *Source: [Wikipedia][1]
 *
 * The baud rate is defined to be the transfer rate in bit per second (bps)
 *   -- *Source: [Atmel AVR Datasheet][2], Page 203*
 *
 *
 * [1]: en.wikipedia.org/wiki/Baud "Wikipedia - Baud"
 * [2]: http://www.atmel.com/Images/Atmel-2549-8-bit-AVR-Microcontroller-ATmega640-1280-1281-2560-2561_datasheet.pdf "2549Q–AVR–02/2014"
 *
 *
 * Generic Baud Rates
 * ------------------
 *
 * -      75 Bd (Teletype)
 * -    2400 Bd
 * -    4800 Bd
 * -    9600 Bd
 * -   31250 Bd (MIDI) *(round multiple to 1MHz)*
 * -   38400 Bd
 * -  115200 Bd
 * -  250000 Bd (DMX)  *(round multiple to 1MHz)*
 *
 *
 * Usage
 * -----
 *
 * ### How to connect a serial terminal
 *
 *	# picocom --baud 9600 --stopbits 2 /dev/ttyUSB0
 */


#include <avr/io.h>
#include <avr/interrupt.h>
#include <mars/utils/fifo.h>
#include <mars/utils/io.h>


/** FIFO buffer size. */
#define BUFFER_SIZE 81 // one terminal line with CR ('\n')


uint8_t buf_in[BUFFER_SIZE];  ///< Input buffer.
uint8_t buf_out[BUFFER_SIZE]; ///< Output buffer.
fifo_t fifo_in;               ///< Input FIFO data.
fifo_t fifo_out;              ///< Output FIFO data.


/** Interrupt Service Routine - Receive Complete interrupt vector. */
ISR(USARTx_RX_vect)
{
	_inline_fifo_put(&fifo_in, UDRx);
}


/** Interrupt Service Routine - USART Data Register Empty interrupt vector. */
ISR(USARTx_UDRE_vect)
{
	if (fifo_out.count > 0) {
		UDRx = _inline_fifo_get(&fifo_out);
	} else {
		UCSRxB &= ~(1<<UDRIEx); // disable this interrupt
	}
}


/** Initialize UART interface for IRQ.
 *
 * @param ubrr factor between b_uart and f_cpu (for calculation see below)
 *
 *
 * Setup
 * -----
 *
 * - Enable receiver, transmitter and interrupt
 * - Frame format: 8 data bit, 2 stop bit, no parity bit, asynchronous (8N2)
 *
 *
 * About UBRR (USART Baud Rate Register)
 * -------------------------------------
 *
 * - For the UBRR value read the Datasheet, Chapter "Examples of Baud Settings"
 *   - E.g. Document 254Q-AVR-02/14, Page 223
 * - For calculation use the function `uart_calc_ubbr()` from file `uart_utils.h`
 *
 *
 * ### Calculation
 *
 * - ubrr    = ROUND((f_cpu / (16 * baud_e)) - 1)
 * - baud_a  = f_cpu / (16 * (ubbr + 1))
 * - err_rel = ((baud_a / baud_e ) - 1) * 100%
 *
 * - baud_e := exact baud rate
 * - baud_a := approximated  baud rate
 *
 * - by standard, the relative error (err_rel) has to be between +0.5% and -0.5%,
 * - ubrr [0-4095] *(value range)*
 */
//void uart_init(uint16_t ubbr, uint8_t paritybits, uint8_t databits, uint8_t stopbits)
int8_t uart_irq_init(uint16_t ubrr)
{
	uint8_t sreg = SREG; // save global status register
	cli();               // clear interrupt flag (in SREG)

	UBRRxH = (uint8_t) (ubrr>>8);
	UBRRxL = (uint8_t) ubrr;

	UCSRxB = (1<<RXENx)|(1<<TXENx)|(1<<RXCIEx);  // enable receiver, transmitter and receiver interrupt
	UCSRxC = (1<<USBSx)|(1<<UCSZx1)|(1<<UCSZx0); // set frame format: 8N2, asynchronous

	do { // flush receive buffer register
		UDRx;
	} while (UCSRxA & (1<<RXCx));

//	UCSRxA = (1<<RXCx)|(1<<TXCx); // TODO: proof that: according to the datasheet TXCx should be '0'
	UCSRxA = (1<<RXCx); // TEST

	SREG = sreg; // restore global status register with global interrupt flag

	fifo_init(&fifo_in, buf_in, BUFFER_SIZE);
	fifo_init(&fifo_out, buf_out, BUFFER_SIZE);

	return 0;
}


/** UART IRQ - Transmit Byte.
 *
 * This function puts a byte to the OUT FIFO.
 * Then it enables the UDRIEx IRQ.
 * The ISR(USARTx_UDRE_vect) transmits the byte from the FIFO.
 *
 * @param  b byte to transmit, put the OUT FIFO
 * @return   transmitted byte, casted to int
 * @retval   ERR_FIFO_FULL if OUT FIFO is full
 */
int uart_irq_transmit_byte(uint8_t b)
{
	int ret = fifo_put(&fifo_out, b);
	UCSRxB |= (1<<UDRIEx);
	return ret;
}


/** UART IRQ - Receive Byte - wait till receiving a byte.
 *
 * This function returns the next byte from the IN FIFO.
 * If there is no byte, it waits till there is one.
 *
 * @return received byte, got from the IN FIFO
 */
uint8_t uart_irq_receive_byte_wait(void)
{
	return fifo_get_wait(&fifo_in);
}


/** UART IRQ - Receive Byte - do not wait till receiving a byte.
 *
 * This function returns the next byte from the IN FIFO.
 * If there is no byte, it returns -1.
 *
 * @return    received byte, got from the IN FIFO
 * @retval -1 no byte in the IN FIFO
 */
int uart_irq_receive_byte_nowait(void)
{
	return fifo_get_nowait(&fifo_in);
}


/** <stdio.h> compatible implementation of "putchar()" for USART with IRQ.
 *
 * This function uses "io_transmit()" from the <mars/utils/io.h> library.
 * "io_transmit()" handles the new line sequence, OS specific.
 *
 * @param  c    character to transmit, put the OUT FIFO
 * @param  FILE stream pointer (not used)
 * @return      transmitted character byte, casted to int
 * @retval      ERR_FIFO_FULL if OUT FIFO is full
 */
int uart_irq_putchar(char c, FILE* stream)
{
	return io_transmit(c, &uart_irq_transmit_byte);
}


/** <stdio.h> compatible implementation of "getchar()" for USART with IRQ, waiting for input.
 *
 * This function uses "io_receive_byte()" from the <mars/utils/io.h> library.
 * "io_receive_byte()" handles the new line sequence, OS specific.
 *
 * @param  FILE stream pointer (not used)
 * @return      received character byte, casted to int
 */
int uart_irq_getchar_wait(FILE *stream)
{
	return (int) io_receive_byte(&uart_irq_receive_byte_wait, &uart_irq_transmit_byte);
}


/** <stdio.h> compatible implementation of "getchar()" for USART with IRQ, not waiting for input.
 *
 * This function uses "io_receive()" from the <mars/utils/io.h> library.
 * "io_receive()" handles the new line sequence, OS specific.
 *
 * @param  FILE stream pointer (not used)
 * @return      received character byte, casted to int
 */
int uart_irq_getchar_nowait(FILE *stream)
{
	return io_receive(&uart_irq_receive_byte_nowait, &uart_irq_transmit_byte);
}

