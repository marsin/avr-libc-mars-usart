/******************************************************************************
 Atmel - C driver - UART
   - UART driver, implemented in C for Atmel microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Polling UART/USART, Interface 2 driver.
 *
 * @file      uart2_poll.c
 * @see       uart2_poll.h
 * @see       uart_poll.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


// Functions
#define uart_poll_init uart2_poll_init

#define uart_poll_transmit_byte       uart2_poll_transmit_byte
#define uart_poll_receive_byte_wait   uart2_poll_receive_byte_wait
#define uart_poll_receive_byte_nowait uart2_poll_receive_byte_nowait

#define uart_poll_putchar        uart2_poll_putchar
#define uart_poll_getchar_wait   uart2_poll_getchar_wait
#define uart_poll_getchar_nowait uart2_poll_getchar_nowait

// Register
#define UBRRxH UBRR2H
#define UBRRxL UBRR2L

#define UCSRxA UCSR2A
#define UCSRxB UCSR2B
#define UCSRxC UCSR2C

#define UDRx UDR2

// Bits
#define RXENx RXEN2
#define TXENx TXEN2
#define USBSx USBS2
#define UCSZx0 UCSZ20

#define UDREx UDRE2
#define RXCx RXC2


#include "uart2_poll.h"
#include "uart_poll.c"

