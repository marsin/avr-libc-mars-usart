/******************************************************************************
 Atmel - C driver - UART
   - UART driver, implemented in C for Atmel microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** UART/USART with IRQ, Interface 2 driver.
 *
 * @file      uart2_irq.c
 * @see       uart2_irq.h
 * @see       uart_irq.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


// ISR
#define USARTx_RX_vect USART2_RX_vect
#define USARTx_UDRE_vect USART2_UDRE_vect

// Functions
#define uart_irq_init uart2_irq_init

#define uart_irq_transmit_byte       uart2_irq_transmit_byte
#define uart_irq_receive_byte_wait   uart2_irq_receive_byte_wait
#define uart_irq_receive_byte_nowait uart2_irq_receive_byte_nowait

#define uart_irq_putchar        uart2_irq_putchar
#define uart_irq_getchar_wait   uart2_irq_getchar_wait
#define uart_irq_getchar_nowait uart2_irq_getchar_nowait

// Register
#define UBRRxH UBRR2H
#define UBRRxL UBRR2L

#define UCSRxA UCSR2A
#define UCSRxB UCSR2B
#define UCSRxC UCSR2C

#define UDRx UDR2

// Bits
#define RXENx RXEN2
#define TXENx TXEN2
#define USBSx USBS2
#define UCSZx0 UCSZ20
#define UCSZx1 UCSZ21
#define RXCIEx RXCIE2
#define UDRIEx UDRIE2

#define RXCx RXC2
#define TXCx TXC2


#include "uart2_irq.h"
#include "uart_irq.c"

