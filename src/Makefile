# Atmel - C driver
# ================
#
#   - driver development in C for Atmel microcontroller,
#       using the MyAVR MK3 development board
#
# Copyright (c) 2017 Martin Singer <martin.singer@web.de>
#
# This file is part of myAVR-MK3_c-driver_libuart
#
#
# About this Makefile
# -------------------
#
# WinAVR makefile written by Eric B. Weddington, Jörg Wunsch, et al.
# Released to the Public Domain.
# Please read the make user manual!
#
# Additional material for this makefile was submitted by:
# * Tim Henigan
# * Peter Fleury
# * Reiner Patommel
# * Sander Pool
# * Frederik Rouleau
# * Markus Pfaff
# * Martin Singer
#
#
# Usage
# -----
#
# * `make all`        Make software.
# * `make clean`      Clean out built project files.
# * `make coff`       Convert ELF to AVR COFF
#                       (for use with AVR Studio 3.x or VMLAB).
# * `make extcoff`    Convert ELF to AVR Extended COFF
#                       (for use with AVR Studio 4.07 or greater).
# * `make program`    Download the hex file to the device, using avrdude.
#                       Please customize the avrdude settings below first!
# * `make filename.s` Just compile filename.c into the assembler code only.
# * `make install`    Install drivers as library on your system.
# * `make uninstall`  Uninstall driver library from your system.
#
# To rebuild project do `make clean` then `make all`.
#
#
# Information
# -----------
#
#	$ man avr-gcc
#	$ avrdude -c


## Microcontroller Unit.
# Edit this for creating and installing the lib for different controllers.
# The ISA variable (Instruction Set Architecture) depends on this variable.
MCU = atmega2560
#MCU = atmega32u4
#MCU = atmega8


## Central Processing Unit frequency.
# (used for define F_CPU in all assembler and C sources)
F_CPU = 16000000ul
#F_CPU = 3686400ul


## Output fromat.
# [srec, ihex, binary]
FORMAT = ihex


## Target filename.
# (also used for the library package name and as library include directory name)
TARGET = uart


## List of libraries used to link the target.
#LIBS =
LIBS = mk3 utils


## List of extra directories to look for include and link files.
EXTRADIRS =


## List of C source files.
# Required for compiling and programming the IC.
# (not required for compiling and installing as library)
CSRC = \
	uart_utils.c \
	uart0_poll.c \
	uart1_poll.c \
	uart2_poll.c \
	uart3_poll.c \
	uart0_irq.c \
	uart1_irq.c \
	uart2_irq.c \
	uart3_irq.c \
	test.c \
	main.c


## List of Assembler source files.
# (use .S, don't use .s for file extension)
ASRC =


## Selection of TEST function.
#
# About
# -----
#
# This macro selects which TEST function is to compile and use.
# For more information about a particular function, read the comments in `test.c`,
# directly in the source file or create the _doxygen_ reference.
#
#
# Usage
# -----
#
# You can define the TEST function constant here,
# and you can override it with the make command (e.g.: `make TEST=1`)
#
# __IMPORTANT__: Execute `make clean` after changing the TEST function!
#
#
# TEST functions
# --------------
#
# 1. test_uart3_poll_ping()
# 2. test_uart3_poll_hello_world()
# 3. test_uart3_poll_hello_stream()
# 4. test_uart3_poll_echo()
# 5. test_uart3_irq_ping()
# 6. test_uart3_irq_echo()
TEST = 6


## AVR Instruction Set Architecture to create the library for.
#
# Selects ISA depending on the MCU.
#
#
# About 'strip', 'patsubst' and 'findstring' in Makefiles:
#
# * <https://gcc.gnu.org/onlinedocs/gcc/AVR-Options.html>
ISA  = \
	$(strip \
		$(patsubst atmega8,   avr4,$(findstring atmega8,   $(MCU))) \
		$(patsubst atmega32u4,avr5,$(findstring atmega32u4,$(MCU))) \
		$(patsubst atmega2560,avr6,$(findstring atmega2560,$(MCU))) \
	)

LIB_ARCH = $(if $(ISA),$(ISA),$(MCU))


## List of source files to create the library from.
# Required for installation as library file on the system.
# (not required for programming the IC)
LIB_SRC = \
	uart_utils.c \
	uart0_poll.c \
	uart1_poll.c \
	uart2_poll.c \
	uart3_poll.c \
	uart0_irq.c \
	uart1_irq.c \
	uart2_irq.c \
	uart3_irq.c


## List of include (header) files of the library.
# Required for installing as include files on the system.
# (not required for programming the IC)
LIB_INC = \
	uart_utils.h \
	uart0_poll.h \
	uart1_poll.h \
	uart2_poll.h \
	uart3_poll.h \
	uart0_irq.h \
	uart1_irq.h \
	uart2_irq.h \
	uart3_irq.h


## Optimization level.
# [0, 1, 2, 3, s]
# (see avr-libc FAQ)
OPT = s
#OPT = 1


## Debugging format.
# Native formats for AVR-GCC's -g are `stabs` (default) or `dwarf-2`.
# AVR (extended) `coff` requires stabs, plus an avr-objcopy run.
# `stabs+` uses GNU extensions, understood only by the GNU debugger (GDB).
DEBUG = stabs+


## C Standard.
CSTANDARD = -std=gnu11


## -D or -U options.
CDEFS =


## -I options.
CINCS =


## C Compiler flags.
#  -g*:          generate debugging information
#  -O*:          optimization level
#  -f...:        tuning, see GCC manual and avr-libc documentation
#  -Wall...:     warning level
#  -Wa,...:      tell GCC to pass this to the assembler.
#    -adhlns...: create assembler listing
CFLAGS  = -g$(DEBUG)
CFLAGS += $(CDEFS)
CFLAGS += $(CINCS)
CFLAGS += -O$(OPT)
CFLAGS += -funsigned-char
CFLAGS += -funsigned-bitfields
CFLAGS += -fpack-struct
CFLAGS += -fshort-enums
CFLAGS += -Wall
CFLAGS += -Wstrict-prototypes
CFLAGS += -Wa,-adhlns=$(<:.c=.lst)
#CFLAGS += -I$(DIR_AVR_INC)
CFLAGS += $(patsubst %,-I%,$(EXTRADIRS))
CFLAGS += $(CSTANDARD)
CFLAGS += -DF_CPU=$(F_CPU)
CFLAGS += -DTEST=$(TEST)


## Assembler flags.
#  -Wa,...:   tell GCC to pass this to the assembler.
#    -ahlms:  create listing
#    -gstabs: have the assembler create line number information; note that
#             for use in COFF files, additional information about filenames
#             and function names needs to be present in the assembler source
#             files -- see avr-libc docs [FIXME: not yet described there]
ASFLAGS  = -Wa,-adhlns=$(<:.S=.lst),-gstabs
ASFLAGS += -DF_CPU=$(F_CPU)
ASFLAGS += -DTEST=$(TEST)


## Aditional libraries.

# Minimalistic printf version
PRINTF_LIB_MIN = -Wl,-u,vfprintf -lprintf_min

# Floating point printf version
# (requires MATH_LIB = -lm below)
PRINTF_LIB_FLOAT = -Wl,-u,vfprintf -lprintf_flt

# Minimalistic scanf version
SCANF_LIB_FLOAT = -Wl,-u,vfscanf -lscanf_min

# Floating point + %[ scanf version
# (requires MATH_LIB = -lm below)
SCANF_LIB_FLOAT = -Wl,-u,vfscanf -lscanf_flf

PRINTF_LIB =
SCANF_LIB =
MATH_LIB =
#MATH_LIB = -lm


# External memory options

# 64 KB of external RAM, starting after internal RAM (ATmega128!),
# used for variables (.data/.bss) and heap (malloc()).
#EXTMEMOPTS = -Wl,-Tdata=0x801100,--defsym=__heap_end=0x80ffff

# 64 KB of external RAM, starting after internal RAM (ATmega128!),
# only used for heap (malloc()).
#EXTMEMOPTS = -Wl,--defsym=__heap_start=0x801100,--defsym=__heap_end=0x80ffff

EXTMEMOPTS =


## Linker flags.
#   -Wl,...:  tell GCC to pass this to linker
#     -Map:   create map file
#     --cref: add cross reference to map file
LDFLAGS  = -Wl,-Map=$(TARGET).map,--cref
LDFLAGS += $(EXTMEMOPTS)
LDFLAGS += $(PRINTF_LIB) $(SCANF_LIB) $(MATH_LIB)
#LDFLAGS += -L$(DIR_AVR_LIB)
LDFLAGS += $(patsubst %,-l%,$(LIBS))


# Programming support using `avrdude`
# -----------------------------------

## Programming hardware.
# Type: `avrdude -c` to get a full listing.
#
# * `avr911`:     MySmartUSB MK2
# * `stk500v2`:   MySmartUSB MK3
# * `avr911`:     Arduino USB Bootloader (e.g. Arduiono Micro - ATmega32U4)
# * `avrispmkII`: AVR ISP MKII (Diamex ALL AVR)
AVRDUDE_PROGRAMMER = avrispmkII
#AVRDUDE_PROGRAMMER = avrispmkII
#AVRDUDE_PROGRAMMER = avr911


## Programmer ingerface port.
#
# * `/dev/ttyUSB0`: MySmartUSB MK2
# * `/dev/ttyUSB0`: MySmartUSB MK3
# * `/dev/ttyACM0`: AVR USB Bootloader (e.g. ATmega32U4)
# * usb           : AVR ISP MKII (Diamex ALL AVR)
#AVRDUDE_PORT = /dev/ttyUSB0
#AVRDUDE_PORT = /dev/ttyACM0
#AVRDUDE_PORT = /dev/tty.mk3
AVRDUDE_PORT = usb


## Programmer speed.
#
# * `57600`: AVR USB Bootloader (e.g. ATmega32U4)
#AVRDUDE_BAUDRATE = -b 57600


AVRDUDE_WRITE_FLASH = -U flash:w:$(TARGET).hex
#AVRDUDE_WRITE_EEPROM = -U eeprom:w:$(TARGET).eep

#AVRDUDE_WRITE_FLASH = -U flash:w:$(TARGET).hex:i
#AVRDUDE_WRITE_EEPROM = -U eeprom:w:$(TARGET).eep:i


# Uncomment the following if you want avrdude's erase cycle counter.
# Note that this counter needs to be initialized first using -Yn,
# see avrdude manual.
#AVRDUDE_ERASE_COUNTER = -y


# Uncomment the following if you do /not/ wish a verification to be
# performed after programming the device.
#AVRDUDE_NO_VERIFY = -V


## Skip Device Signature Check.
# (when signature is broken - erased or overwritten)
#AVRDUDE_SKIP_SIGNATURE = -F


# Increase verbosity level.  Please use this when submitting bug
# reports about avrdude. See <http://savannah.nongnu.org/projects/avrdude>
# to submit bug reports.
AVRDUDE_VERBOSE = -v -v
#AVRDUDE_VERBOSE = -v -v -v -v


AVRDUDE_FLAGS = -p $(MCU) -P $(AVRDUDE_PORT) -c $(AVRDUDE_PROGRAMMER)
AVRDUDE_FLAGS += $(AVRDUDE_BAUDRATE)
AVRDUDE_FLAGS += $(AVRDUDE_NO_VERIFY)
AVRDUDE_FLAGS += $(AVRDUDE_SKIP_SIGNATURE)
AVRDUDE_FLAGS += $(AVRDUDE_VERBOSE)
AVRDUDE_FLAGS += $(AVRDUDE_ERASE_COUNTER)


# ---------------------------------------------------------------------

## Define directories.
#DIR_AVR = /opt/cross
DIR_AVR = /usr/local
#DIR_AVR_BIN = $(DIR_AVR)/bin
#DIR_AVR_UTILS = $(DIR_AVR)/utils/bin
DIR_AVR_LIB = $(DIR_AVR)/avr/lib
DIR_AVR_INC = $(DIR_AVR)/avr/include


## Define library name and header path for installing on the system.
# (not required for programming the IC)
TARGET_LIB = lib$(TARGET).a
#TARGET_LIB_HDR = $(DIR_AVR_INC)/$(TARGET)


## Define programs and commands.
SHELL = sh
CC = avr-gcc
OBJCOPY = avr-objcopy
OBJDUMP = avr-objdump
SIZE = avr-size
NM = avr-nm
AVRDUDE = avrdude
REMOVE = rm -f
COPY = cp
INSTALL	= install --mode 644 -D
REMOVEDIR = rmdir
ARCHIVER = avr-ar -rcs


## Define messages.
MSG_ERRORS_NONE = Errors none
MSG_BEGIN = -------- begin --------
MSG_END = --------  end  --------
MSG_SIZE_BEFORE = Size bevore: 
MSG_SIZE_AFTER = Size after: 
MSG_COFF = Converting to AVR COFF: 
MSG_EXTENDED_COFF = Converting to AVR extended COFF: 
MSG_FLASH = Creating load file for flash: 
MSG_EEPROM = Creating load file for EEPROM: 
MSG_EXTENDED_LISTING = Creating extended listing: 
MSG_SYMBOL_TABLE = Creating symbol table: 
MSG_LINKING = Linking: 
MSG_COMPILING = Compiling: 
MSG_ASSEMBLING = Assembling: 
MSG_CLEANING = Cleaning: 
MSG_ARCHIVING = Crating library archive 
MSG_INSTALLING = Installing library and headers on system 


## Define all object files.
OBJ = $(CSRC:.c=.o) $(ASRC.S=o)


## Define all listing files.
LST = $(CSRC:.c=.lst) $(ASRC:.S=.lst)


## Define all library object files
# (not required for programming the IC)
LIB_OBJ = $(LIB_SRC:.c=.o)


## Compiler flags to generate dependency files.
#GENDEPFLAGS = -Wp,-M,-MP,-MT,$(*F).o,-MF,.dep/$(@F).d
GENDEPFLAGS = -MD -MP -MF .dep/$(@F).d


## Combine all necessary flags and optional flags.
# Add target processor to flags
#ALL_CFLAGS = -mmcu=$(MCU) -I. $(CFLAGS) $(GENDEPFLGS)
#ALL_ASFLAGS = -mmcu=$(MCU) -I. -x assembler-with-cpp $(ASFLAGS)
ALL_CFLAGS =  $(strip -mmcu=$(MCU) -I. -I$(DIR_AVR_INC) $(CFLAGS) $(GENDEPFLGS))
ALL_ASFLAGS = $(strip -mmcu=$(MCU) -I. -I$(DIR_AVR_INC) -x assembler-with-cpp $(ASFLAGS))
ALL_LDFLAGS = $(strip $(LDFLAGS) -L$(DIR_AVR_LIB) -L$(DIR_AVR_LIB)/$(LIB_ARCH) -L$(DIR_AVR_LIB)/$(LIB_ARCH)/mars)


## Default target.
all: begin gccversion sizebefore build sizeafter finished end

## Library
library: clean begin gccversion sizebefore lib sizeafter finished end


build: elf hex epp lss sym


elf: $(TARGET).elf
hex: $(TARGET).hex
epp: $(TARGET).eep
lss: $(TARGET).lss
sym: $(TARGET).sym
lib: $(TARGET_LIB)


## Messages.
begin:
	@echo
	@echo $(MSG_BEGIN)

finished:
	@echo $(MSG_ERRORS_NONE)

end:
	@echo $(MSG_END)
	@echo


## Display size of file.
HEXSIZE = $(SIZE) --target=$(FORMAT) $(TARGET).hex
ELFSIZE = $(SIZE) -A $(TARGET).elf

sizebefore:
	@if [ -f $(TARGET).elf ]; then echo; echo $(MSG_SIZE_BEFORE); $(ELFSIZE); echo; fi

sizeafter:
	@if [ -f $(TARGET).elf ]; then echo; echo $(MSG_SIZE_AFTER); $(ELFSIZE); echo; fi


## Display compiler version information.
gccversion:
	@$(CC) --version


## Programm the device.
program: $(TARGET).hex $(TARGET).eep
	$(AVRDUDE) $(AVRDUDE_FLAGS) $(AVRDUDE_WRITE_FLASH) $(AVRDUDE_WRITE_EEPROM)


## Install libraries and include (header) files on system
install: begin install_library end

## Uninstall libraries and include (header) files from system
uninstall: begin uninstall_library end


## Convert ELF to COFF for use in debugging / simulating in AVR Studio or VMLAB.
COFFCONVERT = $(OBJCOPY) --debugging \
	--change-section-address .data-0x800000 \
	--change-section-address .bss-0x800000 \
	--change-section-address .noinit-0x800000 \
	--change-section-address .eeprom-0x810000

coff: $(TARGET).elf
	@echo
	@echo $(MSG_COFF) $(TARGET).cof
	$(COFFCONVERT) -O coff-avr $< $(TARGET).cof

extcoff: $(TARGET).elf
	@echo
	@echo $(MSG_EXTENDED_COFF) $(TARGET).cof
	$(COFFCONVERT) -O coff-ext-avr $< $(TARGET).cof


## Create final output files (.hex, .eep) from ELF output file.
%.hex: %.elf
	@echo
	@echo $(MSG_FLASH) $@
	$(OBJCOPY) -O $(FORMAT) -R .eeprom $< $@

%.eep: %.elf
	@echo
	@echo $(MSG_FLASH) $@
	$(OBJCOPY) -j .eeprom --set-section-flags=.eeprom="alloc,load" \
	--change-section-lma .eeprom=0 -O $(FORMAT) $< $@


## Create extended listing file from ELF output file.
%.lss: %.elf
	@echo
	@echo $(MSG_EXTENDED_LISTING) $@
	$(OBJDUMP) -h -S $< > $@


## Create a symbol table from ELF output file.
%.sym: %.elf
	@echo
	@echo $(MSG_SYMBOL_TABLE) $@
	$(NM) -n $< > $@


## Link: create ELF output file from object files.
.SECONDARY : $(TARGET).elf
.PRECIOUS : $(OBJ)
%.elf: $(OBJ)
	@echo
	@echo $(MSG_LINKING) $@
	$(CC) $(ALL_CFLAGS) $(OBJ) --output $@ $(ALL_LDFLAGS)
#	$(CC) $(ALL_CFLAGS) $(OBJ) --output $@ $(LDFLAGS) -L$(DIR_AVR_LIB)
#	$(CC) $(ALL_CFLAGS) $(OBJ) --output $@ $(LDFLAGS)


## Archive library object files.
%.a: $(LIB_OBJ)
	@echo
	@echo $(MSG_ARCHIVING) $@
	$(ARCHIVER) $(TARGET_LIB) $(LIB_OBJ)


## Compile: create object files from C source files.
%.o : %.c
	@echo
	@echo $(MSG_COMPILING) $<
	$(CC) -c $(ALL_CFLAGS) $< -o $@


## Compile: create assembler files from C source files.
%.s : %.c
	$(CC) -S $(ALL_CFLAGS) $< -o $@


## Assemble: create object files from assembler source files.
%.o : %.S
	@echo
	@echo $(MSG_ASSEMBLING) $<
	$(CC) -c $(ALL_ASFLAGS) $< -o $@


## Target: clean project.
clean: begin clean_list finished end

clean_list:
	@echo
	@echo $(MSG_CLEANING)
	$(REMOVE) $(TARGET).hex
	$(REMOVE) $(TARGET).eep
	$(REMOVE) $(TARGET).obj
	$(REMOVE) $(TARGET).cof
	$(REMOVE) $(TARGET).elf
	$(REMOVE) $(TARGET).map
	$(REMOVE) $(TARGET).obj
	$(REMOVE) $(TARGET).a90
	$(REMOVE) $(TARGET).sym
	$(REMOVE) $(TARGET).lnk
	$(REMOVE) $(TARGET).lss
	$(REMOVE) $(OBJ)
	$(REMOVE) $(LST)
	$(REMOVE) $(CSRC:.c=.s)
	$(REMOVE) $(CSRC:.c=.d)
	$(REMOVE) $(LIB_OBJ)
	$(REMOVE) $(TARGET_LIB)
	$(REMOVE) .dep/*


## Library installation.
install_library: $(TARGET_LIB) $(LIB_INC)
	@echo
	@echo "$(INSTALL) $(TARGET_LIB) $(DIR_AVR_LIB)/$(LIB_ARCH)/mars/$(TARGET_LIB)" ; \
	       $(INSTALL) $(TARGET_LIB) $(DIR_AVR_LIB)/$(LIB_ARCH)/mars/$(TARGET_LIB)
	@for inc in $(LIB_INC); do \
		echo "$(INSTALL) '$${inc}' '$(DIR_AVR_INC)/mars/$(TARGET)/$${inc}'" ; \
		      $(INSTALL)  $${inc}   $(DIR_AVR_INC)/mars/$(TARGET)/$${inc}   ; \
	done
	@echo

uninstall_library:
	@echo
	@echo "$(REMOVE) $(DIR_AVR_LIB)/$(LIB_ARCH)/mars/$(TARGET_LIB)" ; \
	       $(REMOVE) $(DIR_AVR_LIB)/$(LIB_ARCH)/mars/$(TARGET_LIB)
	@for dir in $$(find $(DIR_AVR_LIB)/$(LIB_ARCH) -type d -empty); do \
		echo "$(REMOVEDIR) '$${dir}'" ; \
		      $(REMOVEDIR)  $${dir}   ; \
	done
	@for inc in $(LIB_INC); do \
		echo "$(REMOVE) '$(DIR_AVR_INC)/mars/$(TARGET)/$${inc}'" ; \
		      $(REMOVE)  $(DIR_AVR_INC)/mars/$(TARGET)/$${inc}   ; \
	done
	@for dir in $$(find $(DIR_AVR_INC) -type d -empty); do \
		echo "$(REMOVEDIR) '$${dir}'" ; \
		      $(REMOVEDIR)  $${dir}   ; \
	done
	@echo


## Include the dependency files.
-include $(shell mkdir .dep 2>/dev/null) $(wildcard .dep/*)


## Listing of phony targets.
.PHONY : all begin finish end sizebefore sizeafter gccversion \
build elf hex eep lss sym coff extcoff \
clean clean_list program \
uninstall_library
