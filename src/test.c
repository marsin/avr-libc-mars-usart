/******************************************************************************
 Atmel - C driver - twi driver - tests
   - myAVR MK3 board driver, implemented in C for Atmel microcontroller
 Copyright (c) 2016 - 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** USART driver tests.
 *
 * @file      test.c
 * @see       test.h
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 *
 *
 * Usage
 * -----
 *
 * ### How to connect a serial terminal
 *
 *	# picocom --baud 9600 --stopbits 2 /dev/ttyUSB0
 */


#ifndef F_CPU
#  define F_CPU 16000000UL ///< myAVR-MK3 (ATmega2560)
#endif
#ifndef B_UART
  #define B_UART  9600UL   ///< UART Baud rate (   9600 Bd)
//#define B_UART  38400UL  ///< UART Baud rate (  38400 Bd)
//#define B_UART  115200UL ///< UART Baud rate ( 115200 Bd)
#endif


#include <stdio.h>         // for fdevopen(), putchar(), getchar(), printf(), scanf()
#include <util/delay.h>    // requires F_CPU
#include <avr/interrupt.h>
#include <mars/mk3/leds.h>
#include <mars/mk3/lcd.h>
#include <mars/mk3/seg7.h>
#include <mars/mk3/switch.h>
#include <mars/mk3/joystick.h>
#include <mars/mk3/lcd_terminal.h>
#include "test.h"


#if TEST == 1
/** Ping.
 *
 * Sends a continuous byte signal (0x00).
 * Useful to debug connection failures.
 *
 *
 * Receive
 * =======
 *
 *	# minicom --device /dev/mk3uart --baudrate 9600 --wrap --displayhex
 *
 *
 * Minicom Setup
 * -------------
 *
 * 1. Press in minicom `Ctrl-A Z P`.
 * 2. Setup `Current: 9600 8N2`.
 */
void test_uart3_poll_ping(void)
{
	uart3_poll_init(uart_calc_ubrr(F_CPU, B_UART));

	while (!0) {
		uart3_poll_transmit_byte(0x00);
		_delay_ms(1000);
	}
}


#elif TEST == 2
/** Prints hello, using puts() */
void test_uart3_poll_hello_world(void)
{
	uart3_poll_init(uart_calc_ubrr(F_CPU, B_UART));
	fdevopen(uart3_poll_putchar, uart3_poll_getchar_wait);

	while (!0) {
		puts("\rHello, I am the UART ...");
		_delay_ms(1000);
	}
}


#elif TEST == 3
/** Prints hello on stdout and stderr. */
void test_uart3_poll_hello_stream(void)
{
	mk3_led_init();
	mk3_seg7_init();
	mk3_switch_init();
	mk3_joystick_init();

	mk3_lcd_init();
	mk3_lcd_clear();
	mk3_lcd_set_pos(0, 0);
	mk3_lcd_light(1);

	// Init the UART 3 for poll mode
	uart3_poll_init(uart_calc_ubrr(F_CPU, B_UART));

	FILE uart_stream = FDEV_SETUP_STREAM(uart3_poll_putchar, uart3_poll_getchar_wait, _FDEV_SETUP_RW);
	FILE lcd_stream = FDEV_SETUP_STREAM(mk3_lcd_terminal_putchar, NULL, _FDEV_SETUP_WRITE);

	stdout = stdin = &uart_stream;
	stderr = &lcd_stream;

	while (!0) {
		fprintf(stdout, "\nHello, I am stdout!");
		fprintf(stderr, "\nHello, I am stderr!");
		_delay_ms(1000);
	}
}


#elif TEST == 4
//#  warning Selected TEST function is not tested yet!
/** Echo stdin (uart, serial terminal) on stderr (LCD).
 *
 * * The driver function `uart_getchar()` receives every single character
 *   and sends it back to the addresser
 *   (the serial terminal shows this echoed characters).
 * * Is the character '\n' received,
 *   the standard stream functions flush their buffer to the target streams
 *   (in this case it's the stderr, connected with the LCD).
 */
void test_uart3_poll_echo(void)
{
	char message[81];

	mk3_led_init();
	mk3_seg7_init();
	mk3_switch_init();
	mk3_joystick_init();

	mk3_lcd_init();
	mk3_lcd_clear();
	mk3_lcd_set_pos(0, 0);
	mk3_lcd_light(1);

	// Init the UART 3 for poll mode
	uart3_poll_init(uart_calc_ubrr(F_CPU, B_UART));

	// Define the stream devices - commit the IO functions for the stdio functionality
	FILE uart_stream = FDEV_SETUP_STREAM(uart3_poll_putchar, uart3_poll_getchar_wait, _FDEV_SETUP_RW);
	FILE lcd_stream = FDEV_SETUP_STREAM(mk3_lcd_terminal_putchar, NULL, _FDEV_SETUP_WRITE);

	// Define the std stream devices
	stdout = stdin = &uart_stream;
	stderr = &lcd_stream;

	// Print greeting message
	fprintf(stderr, "\nHello, I am stderr!\n");
	fprintf(stdout, "\nHello, I am stdout!\n");

	/*
	 * 'io_getchar_wait()' and 'io_getchar_nowait()' transmit the received bytes back
	 * to the PC terminal, which prints it to the terminal output.
	 * 'scanf()' uses the stdin, which is connected to the 'io_getchar' functions,
	 * which are connected to the UART functions.
	 * 'fprintf()' prints the received message to stderr which is connected to the LCD.
	 */
	while (!0) {
		scanf("%80s", message);
		fprintf(stderr, "\n%s", message);
	}
}


#elif TEST == 5
/** Pings the character 'a' to uart, using the IRQ. */
void test_uart3_irq_ping(void)
{
	mk3_led_init();
	mk3_seg7_init();
	mk3_switch_init();
	mk3_joystick_init();

	mk3_lcd_init();
	mk3_lcd_clear();
	mk3_lcd_set_pos(0, 0);
	mk3_lcd_light(1);

	uart_irq_init(uart_calc_ubrr(F_CPU, B_UART));
	sei(); // enable interrupts

	while (!0) {
//		uart_irq_transmit_byte(0x00);
		uart_irq_transmit_byte('a');
		_delay_ms(1000);
	}
}


#elif TEST == 6
/** 
 * @see test_uart_poll_echo()
 */
void test_uart3_irq_echo(void)
{
	char message[81];

	mk3_led_init();
	mk3_seg7_init();
	mk3_switch_init();
	mk3_joystick_init();

	mk3_lcd_init();
	mk3_lcd_clear();
	mk3_lcd_set_pos(0, 0);
	mk3_lcd_light(1);

	// Init the UART 3 for IRQ mode
	uart3_irq_init(uart_calc_ubrr(F_CPU, B_UART));

	sei(); // enable interrupts

	FILE uart_stream = FDEV_SETUP_STREAM(uart3_irq_putchar, uart3_irq_getchar_wait, _FDEV_SETUP_RW);
	FILE lcd_stream = FDEV_SETUP_STREAM(mk3_lcd_terminal_putchar, NULL, _FDEV_SETUP_WRITE);

	stdout = stdin = &uart_stream;
	stderr = &lcd_stream;

	fprintf(stderr, "\nHello, I am stderr!\n");
	fprintf(stdout, "\nHello, I am stdout!\n");

	while (!0) {
		scanf("%80s", message);
		fprintf(stderr, "\n%s", message);
//		_delay_ms(1000);
	}
}
#endif // TEST

