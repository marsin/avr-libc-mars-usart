/******************************************************************************
 Atmel - C driver - UART
   - UART driver, implemented in C for Atmel microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** UART/USART with IRQ, Interface 1 driver.
 *
 * @file      uart1_irq.c
 * @see       uart1_irq.h
 * @see       uart_irq.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


// ISR
#define USARTx_RX_vect USART1_RX_vect
#define USARTx_UDRE_vect USART1_UDRE_vect

// Functions
#define uart_irq_init uart1_irq_init

#define uart_irq_transmit_byte       uart1_irq_transmit_byte
#define uart_irq_receive_byte_wait   uart1_irq_receive_byte_wait
#define uart_irq_receive_byte_nowait uart1_irq_receive_byte_nowait

#define uart_irq_putchar        uart1_irq_putchar
#define uart_irq_getchar_wait   uart1_irq_getchar_wait
#define uart_irq_getchar_nowait uart1_irq_getchar_nowait

// Register
#define UBRRxH UBRR1H
#define UBRRxL UBRR1L

#define UCSRxA UCSR1A
#define UCSRxB UCSR1B
#define UCSRxC UCSR1C

#define UDRx UDR1

// Bits
#define RXENx RXEN1
#define TXENx TXEN1
#define USBSx USBS1
#define UCSZx0 UCSZ10
#define UCSZx1 UCSZ11
#define RXCIEx RXCIE1
#define UDRIEx UDRIE1

#define RXCx RXC1
#define TXCx TXC1


#include "uart1_irq.h"
#include "uart_irq.c"

