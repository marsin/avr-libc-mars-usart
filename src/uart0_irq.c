/******************************************************************************
 Atmel - C driver - UART
   - UART driver, implemented in C for Atmel microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** UART/USART with IRQ, Interface 0 driver.
 *
 * @file      uart0_irq.c
 * @see       uart0_irq.h
 * @see       uart_irq.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


// ISR
#define USARTx_RX_vect USART0_RX_vect
#define USARTx_UDRE_vect USART0_UDRE_vect

// Functions
#define uart_irq_init uart0_irq_init

#define uart_irq_transmit_byte       uart0_irq_transmit_byte
#define uart_irq_receive_byte_wait   uart0_irq_receive_byte_wait
#define uart_irq_receive_byte_nowait uart0_irq_receive_byte_nowait

#define uart_irq_putchar        uart0_irq_putchar
#define uart_irq_getchar_wait   uart0_irq_getchar_wait
#define uart_irq_getchar_nowait uart0_irq_getchar_nowait

// Register
#define UBRRxH UBRR0H
#define UBRRxL UBRR0L

#define UCSRxA UCSR0A
#define UCSRxB UCSR0B
#define UCSRxC UCSR0C

#define UDRx UDR0

// Bits
#define RXENx RXEN0
#define TXENx TXEN0
#define USBSx USBS0
#define UCSZx0 UCSZ00
#define UCSZx1 UCSZ01
#define RXCIEx RXCIE0
#define UDRIEx UDRIE0

#define RXCx RXC0
#define TXCx TXC0


#include "uart0_irq.h"
#include "uart_irq.c"

