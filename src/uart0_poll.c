/******************************************************************************
 Atmel - C driver - UART
   - UART driver, implemented in C for Atmel microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Polling UART/USART, Interface 0 driver.
 *
 * @file      uart0_poll.c
 * @see       uart0_poll.h
 * @see       uart_poll.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


// Functions
#define uart_poll_init uart0_poll_init

#define uart_poll_transmit_byte       uart0_poll_transmit_byte
#define uart_poll_receive_byte_wait   uart0_poll_receive_byte_wait
#define uart_poll_receive_byte_nowait uart0_poll_receive_byte_nowait

#define uart_poll_putchar        uart0_poll_putchar
#define uart_poll_getchar_wait   uart0_poll_getchar_wait
#define uart_poll_getchar_nowait uart0_poll_getchar_nowait

// Register
#define UBRRxH UBRR0H
#define UBRRxL UBRR0L

#define UCSRxA UCSR0A
#define UCSRxB UCSR0B
#define UCSRxC UCSR0C

#define UDRx UDR0

// Bits
#define RXENx RXEN0
#define TXENx TXEN0
#define USBSx USBS0
#define UCSZx0 UCSZ00

#define UDREx UDRE0
#define RXCx RXC0


#include "uart0_poll.h"
#include "uart_poll.c"

