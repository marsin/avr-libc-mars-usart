/******************************************************************************
 Atmel - C driver - UART
   - UART driver, implemented in C for Atmel microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Main program.
 *
 * @file      main.c
 * @date      2017
 * @author    Martin Singer
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#include "test.h"


#ifndef TEST
  #define TEST 0
#endif


/** Main function.
 *
 * Choose one of the test functions by editing the variable TEST in the Makefile,
 * or directly with the make command.
 *
 * e.g.
 *
 *	make clean && make TEST=1
 */
int main(void)
{
#if TEST == 1
	test_uart3_poll_ping();
#elif TEST == 2
	test_uart3_poll_hello_world();
#elif TEST == 3
	test_uart3_poll_hello_stream();
#elif TEST == 4
	test_uart3_poll_echo();
#elif TEST == 5
	test_uart3_irq_ping();
#elif TEST == 6
	test_uart3_irq_echo();
#else
  #error No valid TEST function selected!
#endif // TEST

	while (!0) {
		;
	}

	return 0;
}

