/******************************************************************************
 Atmel - C driver - UART
   - UART driver, implemented in C for Atmel microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** UART/USART driver utils.
 *
 * @file      uart_utils.h
 * @see       uart_utils.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#ifndef MARS_UART_UTILS_H
#define MARS_UART_UTILS_H

#include <stdint.h>


/** The maximum UBRR value. */
#define UART_UBRR_MAX 4095


uint16_t uart_calc_ubrr(double, double);


#endif // MARS_UART_UTILS_H

