/******************************************************************************
 Atmel - C driver - UART
   - UART driver, implemented in C for Atmel microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** UART/USART with IRQ, Interface 0 driver.
 *
 * @file      uart0_irq.h
 * @see       uart0_irq.c
 * @see       uart_irq.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#ifndef MARS_UART0_IRQ_H
#define MARS_UART0_IRQ_H


#include <stdio.h>


extern int8_t uart0_irq_init(uint16_t);

// Low level
extern int uart0_irq_transmit_byte(uint8_t);
extern uint8_t uart0_irq_receive_byte_wait(void);
extern int uart0_irq_receive_byte_nowait(void);

// High level
extern int uart0_irq_putchar(char, FILE*);
extern int uart0_irq_getchar_wait(FILE*);
extern int uart0_irq_getchar_nowait(FILE*);


#endif // MARS_UART0_IRQ_H

