/******************************************************************************
 Atmel - C driver - UART
   - UART driver, implemented in C for Atmel microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Polling UART/USART driver.
 *
 * @file      uart_poll.c
 * @see       uart0_poll.h
 * @see       uart0_poll.c
 * @see       uart_utils.h
 * @see       uart_utils.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 *
 *
 * About
 * -----
 *
 * This implementation bases on:
 * <http://rn-wissen.de/wiki/index.php?title=UART_mit_avr-gcc>
 *
 *
 * About Baud
 * ----------
 *
 * Baud is the unit for symbol rate or modulation rate in symbols per second or pulses per second.
 *   -- *Source: [Wikipedia][1]
 *
 * The baud rate is defined to be the transfer rate in bit per second (bps)
 *   -- *Source: [Atmel AVR Datasheet][2], Page 203*
 *
 *
 * [1]: en.wikipedia.org/wiki/Baud "Wikipedia - Baud"
 * [2]: http://www.atmel.com/Images/Atmel-2549-8-bit-AVR-Microcontroller-ATmega640-1280-1281-2560-2561_datasheet.pdf "2549Q–AVR–02/2014"
 *
 *
 * Generic Baud Rates
 * ------------------
 *
 * -      75 Bd (Teletype)
 * -    2400 Bd
 * -    4800 Bd
 * -    9600 Bd
 * -   31250 Bd (MIDI) *(round multiple to 1MHz)*
 * -   38400 Bd
 * -  115200 Bd
 * -  250000 Bd (DMX)  *(round multiple to 1MHz)*
 *
 *
 * Usage
 * -----
 *
 * ### How to connect a serial terminal
 *
 *	# picocom --baud 9600 --stopbits 2 /dev/ttyUSB0
 */


#include <avr/io.h>
#include <mars/utils/io.h>


/** Initialize UART interface for polling.
 *
 * @param ubrr factor between b_uart and f_cpu (for calculation see below)
 *
 *
 * UBRR Calculation
 * ----------------
 *
 * - ubrr    = ROUND((f_cpu / (16 * baud_e)) - 1)
 * - baud_a  = f_cpu / (16 * (ubbr + 1))
 * - err_rel = ((baud_a / baud_e ) - 1) * 100%
 *
 * - baud_e := exact baud rate
 * - baud_a := approximated  baud rate
 *
 * - by standard, the relative error (err_rel) has to be between +0.5% and -0.5%,
 * - ubrr [0-4095] *(value range)*
 *
 *
 * ubrr       |  1000000 Hz |  3686400 Hz |  8000000 Hz | 16000000 Hz | 24000000 Hz | 32000000 Hz
 * ---------- | ----------- | ----------- | ----------- | ----------- | ----------- | -----------
 *      75 Bd |         832 |        3071 |           - |           - |           - |           -
 *     150 Bd |         416 |        1535 |        3332 |           - |           - |           -
 *     300 Bd |         207 |         767 |        1666 |        3332 |           - |           -
 *     600 Bd |         103 |         383 |         832 |        1666 |        2499 |        3332
 *    1200 Bd |          51 |         191 |         416 |         832 |        1249 |        1666
 *    2400 Bd |          25 |          95 |         207 |         416 |         624 |         832
 *    4800 Bd |          12 |          47 |         103 |         207 |         312 |         416
 *    9600 Bd | !!!       6 |          23 |          51 |         103 |         155 |         207
 *   19200 Bd | !!!       2 |          11 |          25 |          51 |          77 |         103
 *   31250 Bd |           1 | !!!       6 |          15 |          31 |          47 |          63
 *   38400 Bd | !!!       1 |           5 |          12 |          25 |          38 |          51
 *  115200 Bd | !!!       0 |           1 | !!!       3 | !!!       8 |          12 | !!!      16
 *  250000 Bd |           - |           0 |           1 |           3 |           5 |           7
 *  500000 Bd |           - |           - |           0 |           1 |           2 |           3
 * 1000000 Bd |           - |           - | !!!       0 |           0 | !!!       1 |           1
 * 2000000 Bd |           - |           - |           - | !!!       0 | !!!       0 |           0
 *
 *
 * err_rel    |  1000000 Hz |  3686400 Hz |  8000000 Hz | 16000000 Hz | 24000000 Hz | 32000000 Hz
 * ---------- | ----------- | ----------- | ----------- | ----------- | ----------- | -----------
 *      75 Bd |       0.04% |       0.00% |           - |           - |           - |           -
 *     150 Bd |      -0.08% |       0.00% |       0.01% |           - |           - |           -
 *     300 Bd |       0.16% |       0.00% |      -0.02% |       0.01% |           - |           -
 *     600 Bd |       0.16% |       0.00% |       0.04% |      -0.02% |       0.00% |       0.01%
 *    1200 Bd |       0.16% |       0.00% |      -0.08% |       0.04% |       0.00% |      -0.02%
 *    2400 Bd |       0.16% |       0.00% |       0.16% |      -0.08% |       0.00% |       0.04%
 *    4800 Bd |       0.16% |       0.00% |       0.16% |       0.16% |      -0.16% |      -0.08%
 *    9600 Bd | !!!  -6.99% |       0.00% |       0.16% |       0.16% |       0.16% |       0.16%
 *   19200 Bd | !!!   8.51% |       0.00% |       0.16% |       0.16% |       0.16% |       0.16%
 *   31250 Bd |       0.00% | !!!   5.33% |       0.00% |       0.00% |       0.00% |       0.00%
 *   38400 Bd | !!! -18.62% |       0.00% |       0.16% |       0.16% |       0.16% |       0.16%
 *  115200 Bd | !!! -45.75% |       0.00% | !!!   8.51% | !!!  -3.55% |       0.16% | !!!   2.12%
 *  250000 Bd |           - | !!!  -7.84% |       0.00% |       0.00% |       0.00% |       0.00%
 *  500000 Bd |           - |           - |       0.00% |       0.00% |       0.00% |       0.00%
 * 1000000 Bd |           - |           - | !!! -50.00% |       0.00% | !!! -25.00% |       0.00%
 * 2000000 Bd |           - |           - |           - | !!! -50.00% | !!! -25.00% |       0.00%
 */
//int8_t uart_init(uint16_t ubbr, uint8_t paritybits, uint8_t databits, uint8_t stopbits)
int8_t uart_poll_init(uint16_t ubrr)
{
	UCSRxB = (1<<RXENx)|(1<<TXENx);  // enable receiver and transmitter
	UCSRxC = (1<<USBSx)|(3<<UCSZx0); // set frame format: 8data, 2stop bit (8N2)

	UBRRxH = (uint8_t) (ubrr>>8);
	UBRRxL = (uint8_t) ubrr;

	return 0;
}


/** UART Poll - Transmit Byte.
 *
 * This function polls the UDREx bit of the UCSRxA register.
 * Is it '1', the byte becomes written to the UDRx register.
 *
 * @param  b byte to transmit
 * @return   transmitted character, type casted to int
 */
int uart_poll_transmit_byte(uint8_t b)
{
	while (! (UCSRxA & (1<<UDREx)));
	UDRx = b;
	return (int) b;
}


/** UART Poll - Receive Byte.
 *
 * This function polls the RXCx bit of the UCSRxA register.
 * Is it '1', the byte becomes read from the UDRx register.
 *
 * @return received byte
 */
uint8_t uart_poll_receive_byte_wait(void)
{
	while (! (UCSRxA & (1<<RXCx)));
	return UDRx;
}


/** UART Poll - Receive Byte (no wait).
 *
 * This function polls **not** the RXCx bit of the UCSRxA register.
 * If RCXx is '1', the byte becomes read from the UDRx register.
 * If RCXx is not '1', the functions returns '-1'.
 *
 * @retval [0..255] received byte
 * @retval -1       no byte received
 */
int uart_poll_receive_byte_nowait(void)
{
	return (UCSRxA & (1<<RXCx)) ? (int) UDRx : -1;
}


/** <stdio.h> compatible implementation of "putchar()" for polling USART.
 *
 * This function uses "io_transmit()" from the <mars/utils/io.h> library.
 * "io_transmit()" handles the new line sequence, OS specific.
 */
int uart_poll_putchar(char c, FILE* stream)
{
	return io_transmit(c, &uart_poll_transmit_byte);
}


/** <stdio.h> compatible implementation of "getchar()" for polling USART, waiting for input.
 *
 * This function uses "io_receive_byte()" from the <mars/utils/io.h> library.
 * "io_receive_byte()" handles the new line sequence, OS specific.
 */
int uart_poll_getchar_wait(FILE *stream)
{
	return (int) io_receive_byte(&uart_poll_receive_byte_wait, &uart_poll_transmit_byte);
}


/** <stdio.h> compatible implementation of "getchar()" for polling USART, not waiting for input.
 *
 * This function uses "io_receive()" from the <mars/utils/io.h> library.
 * "io_receive()" handles the new line sequence, OS specific.
 */
int uart_poll_getchar_nowait(FILE *stream)
{
	return io_receive(&uart_poll_receive_byte_nowait, &uart_poll_transmit_byte);
}
