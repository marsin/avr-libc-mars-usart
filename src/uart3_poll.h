/******************************************************************************
 Atmel - C driver - UART
   - UART driver, implemented in C for Atmel microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Polling UART/USART, Interface 3 driver.
 *
 * @file      uart3_poll.h
 * @see       uart3_poll.c
 * @see       uart_poll.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#ifndef MARS_UART3_POLL_H
#define MARS_UART3_POLL_H


#include <stdio.h>


extern int8_t uart3_poll_init(uint16_t);

// Low level
extern int uart3_poll_transmit_byte(uint8_t);
extern uint8_t uart3_poll_receive_byte_wait(void);
extern int uart3_poll_receive_byte_nowait(void);

// High level
extern int uart3_poll_putchar(char, FILE*);
extern int uart3_poll_getchar_wait(FILE*);
extern int uart3_poll_getchar_nowait(FILE*);


#endif // MARS_UART3_POLL_H

