/******************************************************************************
 Atmel - C driver - twi driver - tests
   - myAVR MK3 board driver, implemented in C for Atmel microcontroller
 Copyright (c) 2016 - 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** USART driver tests.
 *
 * @file      test.h
 * @see       test.c
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#ifndef TEST_MARS_UART_H
#define TEST_MARS_UART_H

#include "uart_utils.h"
#include "uart3_poll.h"
#include "uart3_irq.h"
#include <mars/utils/iostream.h>


#if TEST == 1
void test_uart3_poll_ping(void);
#elif TEST == 2
void test_uart3_poll_hello_world(void);
#elif TEST == 3
void test_uart3_poll_hello_stream(void);
#elif TEST == 4
void test_uart3_poll_echo(void);
#elif TEST == 5
void test_uart3_irq_ping(void);
#elif TEST == 6
void test_uart3_irq_echo(void);
#else
  #error Selected TEST function is not defined!
#endif // TEST


#endif // TEST_MARS_UART_H

