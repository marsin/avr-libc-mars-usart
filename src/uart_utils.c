/******************************************************************************
 Atmel - C driver - UART
   - UART driver, implemented in C for Atmel microcontroller
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** UART/USART driver utils.
 *
 * @file      uart_utils.c
 * @see       uart_utils.h
 * @author    Martin Singer
 * @date      2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#include "uart_utils.h"


/** Calculating the value for the UBRR register from the cpu clock and uart baud.
 *
 * This functions does not check if the values relative error is valid!
 *
 * @param f_cpu  cpu clock speed in Hz
 * @param b_uart uart speed in baud
 * @return UBRR value
 *
 *
 * UBRR Calculation
 * ----------------
 *
 * - ubrr    = ROUND((f_cpu / (16 * baud_e)) - 1)
 * - baud_a  = f_cpu / (16 * (ubbr + 1))
 * - err_rel = ((baud_a / baud_e ) - 1) * 100%
 *
 * - baud_e := exact baud rate
 * - baud_a := approximated  baud rate
 *
 * - by standard, the relative error (err_rel) has to be between +1% and -1%,
 * - ubrr [0-4095] *(value range)*
 */
uint16_t uart_calc_ubrr(double f_cpu, double b_uart)
{
	/*
	 * The formula, given by the manual is
	 * `ubrr = (uint16_t) ((f_cpu / (16.0 * b_uart)) - 1.0)`
	 * By adding 0.5 to the float value,
	 * there will be a correct up and down rounding,
	 * when cutting the decimal places with the typecast from the value.
	 */
	return (uint16_t) ((f_cpu / (16.0 * b_uart)) - 0.5);
}

