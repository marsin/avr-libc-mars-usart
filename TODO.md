ToDo
====

* Implement UART without interrupt					DONE
  - Implement receive_no_wait()						DONE
* Implement UART with interrupt						DONE
* Implement FIFO							DONE
* Offer a callback function hook in UART ISRs
* Make UART0 - UART3 available (don't duplicate code)			DONE
* Source FIFO out to /mars/utils/fifo.h (e.g. for usage in TWI driver)	DONE
* Source IO functions (uartio.h) out to /mars/utils/streamio.h		DONE
  - using a pointer to the output function				DONE
  - Offer NL (new line) handling for different OS'			OUTSOURCED
* Implement USART functions for AVR4 ISR (e.g. ATmega8)
* Check this lib with AVR5 (e.g. ATmega32u4)
