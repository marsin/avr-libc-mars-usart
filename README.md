Atmel AVR C library: UART driver
================================

About
=====

<https://gitlab.com/marsin/myAVR-MK3_c-driver_libuart>

Copyright (c) 2017 Martin Singer <martin.singer@web.de>


Disclaimer
----------

- This is a private project.
- It's under heavy development.
- Don't use it for productive developments.
- It comes WITHOUT ANY WARRANTY!


License
-------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


About the library
-----------------

- This project implements UART driver functions.
- It has some test functions, selected by the compiler directive `TEST`,
  declared in the Makefile or by the `make` command.
- The Makefile is able to install the library functions on the local system.
- The test functions are not required for compiling and installing the library.
- For installing the library for multiple Instruction Set Architectures (ISA)
  edit and use the script `install-libs.sh` (edit the Makefile as well).
- For implementing this project, a myAVR-MK3 development board was/is used,
  but the library is designed for general usage on multiple AVR ISA.


Requirements
============

General
-------

- avr-gcc
- avrdude


Libraries
---------

- The utils library is required for the FIFO and the IO functions.
  <https://gitlab.com/marsin/myAVR_MK3-c-lib-utils>

- The MK3 library is required for the test functions,
  but not for compiling and installing this UART library.
  <https://gitlab.com/marsin/myAVR-MK3_c-driver_libmk3>


Hardware
--------

myAVR MK3 board (only required for the test functions)

- en: <http://shop.myavr.com/?sp=article.sp.php&artID=100063>
- de: <http://shop.myavr.de/?sp=article.sp.php&artID=100063>


Documentation
=============

Creating the Doxygen reference:

	$ doxygen doxygen.conf
	$ firefox doc/html/index.html

- For examples and references have a look at the files `test.h` and `test.c`
- There is usage documentation in the library source files.


Usage
=====

- `make all`        Make software.
- `make clean`      Clean out built project files.
- `make program`    Download the hex file to the device, using avrdude.
- `make install`    Install drivers as library and headers on system.
- `make uninstall`  Uninstall driver library and headers from system.


Installing the library
----------------------

Install drivers as library and headers for multiple ISA on system:

	# sh install-libs.sh


- The library becomes installed under
  - `/usr/local/avr/lib/avr4/mars/libuart.a` *(not supported yet)*
  - `/usr/local/avr/lib/avr5/mars/libuart.a` *(not supported yet)*
  - `/usr/local/avr/lib/avr6/mars/libuart.a`
- The headers become installed under
  - `/usr/local/avr/include/mars/uart/uart.h`
- Include the headers with
  - `#include <mars/uart/uart.h>`
- Link the library with *(depending on your ISA)*
  - `-luart -L/usr/local/avr/lib/avr4/mars` *(not supported yet)*
  - `-luart -L/usr/local/avr/lib/avr5/mars` *(not supported yet)*
  - `-luart -L/usr/local/avr/lib/avr6/mars`


Programming the MC (MK3 board)
------------------------------

Select a test function with the compiler directive TEST,
defined in the Makefile or by the make command.

	$ make clean && make TEST=1 && make install


Configuring the serial device on PC with `udev`
-----------------------------------------------

This configuration is useful to find the USB device (e.g. UART, ISP, ...)
always by the same definite device name.

In general the device appears under `/dev/ttyUSBx`.
Depending if there are other serial USB devices and when the device is connected
devices can change their number (the 'x' in `/dev/ttyUSBx`).
With an udev configuration a device can get recognized by different parameter
like Vendor ID and linked to an individual device name (like `/dev/mk3uart`).


### Recipe

1. Connect the UART-Bridge USB cable with the PC
2. Get information about the serial device, using

    	# lsusb
    	# lsusb -v -s <bus_id>:<device_id>
    	
    	# udevadm info -a -p $(udevadm info -q path -n /dev/<device_name>)

3. Create a udev rule (e.g. `/etc/udev/rules.d/50-uart.rules`)

    Edit the parameter fitting to your device.

    	# MyAVR - MK3 development board UART3
    	KERNEL=="ttyUSB[0-9]", SUBSYSTEMS=="usb", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", ATTRS{product}=="CP2102 USB to UART Bridge Controller", SYMLINK+="tty.mk3-uart"

4. Apply the settings

    	# udevadm control --reload

    or

    	# udevadm trigger

5. Disconnect and Reconnect UART device


Connect the UART with a serial terminal
---------------------------------------

	# picocom --baud 9600 --stopbits 2 /dev/tty.mk3-uart

or

	# minicom --device /dev/tty.mk3-uart --baudrate 9600 --wrap
	# minicom --device /dev/tty.mk3-uart --baudrate 9600 --wrap --displayhex

*(I had problems with sending characters, using minicom)*

